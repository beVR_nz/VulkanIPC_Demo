#include <iostream>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <vector>

#include <vulkan/vulkan.h>

#define SERVER_SOCK_FILE "/tmp/serverTextureIPC"
#define SERVER_SOCK_BUF_SIZE 8192

int sendFD(int socket, int fd);

bool initVulkan();
bool createVulkanImage(uint32_t width, uint32_t height, VkImage& image, VkDeviceMemory& imageMemory);
void transitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout,VkImageAspectFlagBits aspectMask);
uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

VkInstance vulkanInstance;
VkPhysicalDevice physicalDevice;
VkDevice logicalDevice;
VkQueue graphicsQueue;
uint32_t graphicsFamilyIndex;
uint32_t queueFamilyIndex;
VkCommandPool commandPool;

const std::vector<const char*> validationLayers = {
 //   "VK_LAYER_LUNARG_standard_validation",
 //   "VK_LAYER_LUNARG_core_validation"
};

int main() {

    if (! initVulkan())
    {
        printf("Cannot initialise Vulkan. Exiting.\n");
        exit(0);
    }

    //get function pointers for extension functions.
    PFN_vkGetMemoryFdKHR extVkGetMemoryFdKHR  = (PFN_vkGetMemoryFdKHR) vkGetDeviceProcAddr(logicalDevice, "vkGetMemoryFdKHR");

    int textureFD=-1;

    //allocate some vulkan memory, and get a filehandle for it
    VkImage vki;
    VkDeviceMemory vdm;

    createVulkanImage(4096,4096,vki,vdm);
    transitionImageLayout(vki,VK_IMAGE_LAYOUT_UNDEFINED,VK_IMAGE_LAYOUT_GENERAL,VK_IMAGE_ASPECT_COLOR_BIT);
    vkQueueWaitIdle(graphicsQueue);

    VkMemoryGetFdInfoKHR vkFDInfo ={};
    vkFDInfo.sType = VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR;
    vkFDInfo.memory = vdm;
    vkFDInfo.handleType = VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT_KHR;

    if (extVkGetMemoryFdKHR(logicalDevice,&vkFDInfo,&textureFD) != VK_SUCCESS)
    {
            printf("Could not get file descriptor for memory!\n");
            exit(0);
    }
    printf("got FD for texture %d\n",textureFD);

    //create our domain socket

    int fd,cl;
    struct sockaddr_un addr;
    char messageBuffer[SERVER_SOCK_BUF_SIZE];
    struct sockaddr_un from;
    int len;
    socklen_t fromlen = sizeof(from);

    bool socketCreated=true;
    bool socketBound = true;

    if ((fd = socket(PF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        printf("Socket Create Error!\n");
        socketCreated=false;
    }

    if (socketCreated)
    {
        memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, SERVER_SOCK_FILE);
        unlink(SERVER_SOCK_FILE);

        if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        {
            printf("Socket Bind Error!\n");
            socketBound=false;
        }

        if (listen(fd,1024) < 0)
        {
            printf("Listen error!\n");
            socketBound=false;
        }
    }

    if (socketBound)
    {
        // run in an endless loop servicing connections

        while(1)
        {
            if ( (cl = accept(fd, NULL, NULL)) == -1)
            {
                printf("Accept error!\n");
                exit(0);
            }

            int32_t ret = 0;
            while ((len = recvfrom(cl, messageBuffer, SERVER_SOCK_BUF_SIZE, 0, (struct sockaddr *)&from, &fromlen)) > 0)
            {
                messageBuffer[SERVER_SOCK_BUF_SIZE-1] = 0; //zero-pad
                printf ("received Texture Request: %s\n", messageBuffer);
                ret = sendFD(cl, textureFD);
                if (ret < 0)
                {
                    printf("Communication problem!\n");
                    //break;
                }
                printf("Sent FD RESULT: %d!\n",ret);
            }
        }
    }


    if (fd >= 0) {
        close(fd);
    }
    printf("Exiting\n");
    return 0;
}

int sendFD(int socket, int fd)
{
    char dummy = 'P';
    struct msghdr msg;
    struct iovec iov;

    char cmsgbuf[CMSG_SPACE(sizeof(int))];

    iov.iov_base = &dummy;
    iov.iov_len = sizeof(dummy);

    msg.msg_name = NULL;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_flags = 0;
    msg.msg_control = cmsgbuf;
    msg.msg_controllen = CMSG_LEN(sizeof(int));

    struct cmsghdr* cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int));

    *(int*) CMSG_DATA(cmsg) = fd;

    int ret = sendmsg(socket, &msg, 0);

    if (ret == -1) {
        printf("sending FD on socket %d failed with error: %s\n", socket, strerror(errno));
    }

    return ret;
}


bool initVulkan()
{

    uint32_t extensionCount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
    std::vector<VkExtensionProperties> extensions(extensionCount);
    vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
    for (uint32_t i =0;i < extensions.size();i++)
    {
        //printf("VULKAN: available instance extension %s\n",extensions.at(i).extensionName);
    }


    VkApplicationInfo appInfo = {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "TextureServer";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "TextureServer";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo instanceInfo = {};
    instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceInfo.pApplicationInfo = &appInfo;
    std::vector<const char*> requiredInstanceExtensions;

    requiredInstanceExtensions.push_back("VK_KHR_external_memory_capabilities");
    requiredInstanceExtensions.push_back("VK_KHR_external_semaphore_capabilities");
    requiredInstanceExtensions.push_back("VK_KHR_external_fence_capabilities");
    requiredInstanceExtensions.push_back("VK_KHR_get_physical_device_properties2");

    instanceInfo.enabledExtensionCount = requiredInstanceExtensions.size();
    instanceInfo.ppEnabledExtensionNames = requiredInstanceExtensions.data();
    instanceInfo.enabledLayerCount = validationLayers.size();
    instanceInfo.ppEnabledLayerNames = validationLayers.data();
    if (vkCreateInstance(&instanceInfo,nullptr,&vulkanInstance) != VK_SUCCESS)
    {
        printf("ERROR: Could not create Vulkan Instance!\n");
        return false;
    }


    physicalDevice = VK_NULL_HANDLE;

    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(vulkanInstance, &deviceCount, nullptr);
    if (deviceCount == 0)
    {
        printf("ERROR: No Vulkan device available!\n");
        return false;
    }
    std::vector<VkPhysicalDevice> devices(deviceCount);
    vkEnumeratePhysicalDevices(vulkanInstance, &deviceCount, devices.data());

    bool suitableDeviceFound = false;
    for ( unsigned int i=0;i < devices.size();i++ )
    {
        VkPhysicalDeviceProperties deviceProperties;
        VkPhysicalDeviceFeatures deviceFeatures;

        vkGetPhysicalDeviceProperties(devices.at(i), &deviceProperties);
        vkGetPhysicalDeviceFeatures(devices.at(i), &deviceFeatures);

        //TODO: Check Queue Family for VK_GRAPHICS_BIT - the below discrete gpu check is obv not workable for integrated gpu w/vulkan.

        if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
        {
            physicalDevice = devices.at(i);
            suitableDeviceFound=true;
            break;
        }
    }
    if (! suitableDeviceFound)
    {
        printf("ERROR: No suitable Vulkan device on system\n");
        return false;
    }
    uint32_t deviceExtensionCount = 0;
    vkEnumerateDeviceExtensionProperties(physicalDevice,nullptr, &deviceExtensionCount, nullptr);
    std::vector<VkExtensionProperties> deviceExtensions(deviceExtensionCount);
    vkEnumerateDeviceExtensionProperties(physicalDevice,nullptr, &deviceExtensionCount, deviceExtensions.data());
    for (uint32_t i =0;i < deviceExtensions.size();i++)
    {
        //printf("VULKAN: available device extension %s\n",deviceExtensions.at(i).extensionName);
    }

    std::vector<const char*> requiredDeviceExtensions;
    requiredDeviceExtensions.push_back("VK_KHR_external_fence");
    requiredDeviceExtensions.push_back("VK_KHR_external_fence_fd");
    requiredDeviceExtensions.push_back("VK_KHR_external_memory");
    requiredDeviceExtensions.push_back("VK_KHR_external_memory_fd");
    requiredDeviceExtensions.push_back("VK_KHR_external_semaphore");
    requiredDeviceExtensions.push_back("VK_KHR_external_semaphore_fd");
    requiredDeviceExtensions.push_back("VK_KHR_get_memory_requirements2");
    requiredDeviceExtensions.push_back("VK_KHR_dedicated_allocation");

    bool suitableGraphicsFamilyFound = false;
    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, nullptr);

    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueFamilyCount, queueFamilies.data());
    for (unsigned int i=0;i < queueFamilies.size();i++)
    {
        if (queueFamilies.at(i).queueCount > 0 && queueFamilies.at(i).queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            graphicsFamilyIndex = i;
            suitableGraphicsFamilyFound = true;
            break;
        }
    }


    if (! suitableGraphicsFamilyFound)
    {
        printf("ERROR: No suitable Vulkan Graphics Family on device\n");
        return false;
    }



    float queuePriority = 1.0f;
    VkDeviceQueueCreateInfo queueCreateInfo = {};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = graphicsFamilyIndex;
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    VkPhysicalDeviceFeatures physicalDeviceFeatures = {};

    VkDeviceCreateInfo logicalDeviceInfo = {};
    logicalDeviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    logicalDeviceInfo.pQueueCreateInfos = &queueCreateInfo;
    logicalDeviceInfo.queueCreateInfoCount = 1;
    logicalDeviceInfo.pEnabledFeatures = &physicalDeviceFeatures;
    logicalDeviceInfo.enabledExtensionCount=requiredDeviceExtensions.size();
    logicalDeviceInfo.ppEnabledExtensionNames=requiredDeviceExtensions.data();
    logicalDeviceInfo.enabledLayerCount=0;

    if (vkCreateDevice(physicalDevice, &logicalDeviceInfo, nullptr, &logicalDevice) != VK_SUCCESS)
    {
        printf("ERROR: could not create logical device!\n");
        return false;
    }

    vkGetDeviceQueue(logicalDevice, graphicsFamilyIndex, 0, &graphicsQueue);

    //create a command pool for the graphics queue

    VkCommandPoolCreateInfo poolInfo = {};
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = graphicsFamilyIndex;
    poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional

    if (vkCreateCommandPool(logicalDevice, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
    {
        printf("ERROR: could not create command pool!\n");
        return false;
    }

    return true;

}

bool createVulkanImage(uint32_t width, uint32_t height, VkImage& image, VkDeviceMemory& imageMemory)
{
    printf("createVulkanImage IMAGE SIZE: %d x %d\n",width,height);
    VkExternalMemoryImageCreateInfo  imageExternalCreateInfo = {};
    imageExternalCreateInfo.sType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO;
    imageExternalCreateInfo.handleTypes = VK_EXTERNAL_FENCE_HANDLE_TYPE_OPAQUE_FD_BIT;

    VkImageCreateInfo imageInfo = {};
    imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageInfo.pNext = &imageExternalCreateInfo;
    imageInfo.imageType = VK_IMAGE_TYPE_2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageInfo.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT ;
    imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateImage(logicalDevice, &imageInfo, nullptr, &image) != VK_SUCCESS)
    {
        printf("ERROR: failed to create image.\n");
        return false;
    }

    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(logicalDevice, image, &memRequirements);

    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
    uint32_t memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits,VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkMemoryDedicatedAllocateInfoKHR dedicatedAllocInfo = {};
    dedicatedAllocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO_KHR;
    dedicatedAllocInfo.image=image;

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.pNext = &dedicatedAllocInfo;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = memoryTypeIndex;

    if (vkAllocateMemory(logicalDevice, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS) {
        printf("ERROR: failed to allocate image memory.\n");
        return false;
    }

    vkBindImageMemory(logicalDevice, image, imageMemory, 0);

    return true;
}

void transitionImageLayout(VkImage image, VkImageLayout oldLayout, VkImageLayout newLayout,VkImageAspectFlagBits aspectMask)
{

        VkCommandBufferAllocateInfo allocInfo = {};
        allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        allocInfo.commandPool = commandPool;
        allocInfo.commandBufferCount = 1;

        VkCommandBuffer commandBuffer;
        vkAllocateCommandBuffers(logicalDevice, &allocInfo, &commandBuffer);

        VkCommandBufferBeginInfo beginInfo = {};
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

        vkBeginCommandBuffer(commandBuffer, &beginInfo);

        VkImageMemoryBarrier barrier = {};
        barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        barrier.oldLayout = oldLayout;
        barrier.newLayout = newLayout;
        barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        barrier.image = image;
        barrier.subresourceRange.aspectMask = aspectMask;
        barrier.subresourceRange.baseMipLevel = 0;
        barrier.subresourceRange.levelCount = 1;
        barrier.subresourceRange.baseArrayLayer = 0;
        barrier.subresourceRange.layerCount = 1;

        if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_GENERAL)
        {
            barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        }
        else
        {
            printf("ERROR: Unsupported layout transition!\n");
            exit(1);
        }

        vkCmdPipelineBarrier(commandBuffer,VK_PIPELINE_STAGE_ALL_COMMANDS_BIT, VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,0,0, nullptr,0, nullptr,1, &barrier);

        vkEndCommandBuffer(commandBuffer);

        VkSubmitInfo submitInfo = {};
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submitInfo.commandBufferCount = 1;
        submitInfo.pCommandBuffers = &commandBuffer;

        vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
        //TODO: WAITIDLE
        vkQueueWaitIdle(graphicsQueue);

        vkFreeCommandBuffers(logicalDevice, commandPool, 1, &commandBuffer);
    }


uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);

    for (unsigned int i = 0; i < memProperties.memoryTypeCount; i++)
    {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
        {
            return i;
        }
    }

    printf("ERROR: could not find suitable memory type!\n");
    exit(1);
}
