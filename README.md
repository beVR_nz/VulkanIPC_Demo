# VulkanIPC_Demo

Demo/POC code implementing memory allocation with Vulkan in one process and import of the memory in OpenGL from another, using FD-passing over UNIX domain sockets and the external_memory extensions for Vulkan/OpenGL

Build:

I work primarily with Qt Creator, so .pro files are provided for building the executables with Qt Creator or qmake. building with other systems is likely easy enough

Dependencies:

glfw, glm, stb_image - These are typically packaged by distros and should be installable as such.

Usage:

TextureServer must be run first - this program creates a listening UNIX domain socket, and passes a file descriptor back to a client.

TextureClient will connect to the server, and import the allocated memory, passed via FD, as a texture. Run TextureClient with the '-u' or 'update' flag to cause it to upload a texture into
the memory block. Note other instances of the client will immediately update as the memory is now shared cross-process.

TODO: 

Synchronisation of any type is not implemented - this was a bare POC, rather than an attempt to create a robust example.



