#version 330

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 texCoord;

out vec2 fragTexCoord;

uniform mat4 mMatrix;
uniform mat4 vMatrix;
uniform mat4 pMatrix;

void main()
{
    fragTexCoord=texCoord;
    gl_Position = pMatrix * vMatrix * mMatrix * vec4(pos, 1.0);

}
