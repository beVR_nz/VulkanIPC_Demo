#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include "unistd.h"
#include "libgen.h"
#include "gl.h"
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

struct VertexData
{
    glm::vec3 position;
    glm::vec2 texCoord;
};

struct Renderable
{
    std::vector<VertexData> vertices;
    std::vector<uint32_t> indices;
    GLuint vboId;
    GLuint vaoId;
    glm::mat4 modelMatrix;
};

#define SERVER_SOCK_FILE "/tmp/serverTextureIPC"

int recvFD(int socket);
std::string getBasePath();
void checkGLError(char* prefix);
Renderable* createQuad();
bool loadShader(std::vector<char> *buf, const char *file);
GLuint compileShader(GLenum type, const GLchar *source);
GLuint linkShader(GLuint vert, GLuint frag);

std::vector<Renderable*> renderables;
Renderable* fsQuad;
GLuint fsQuadShader;

std::string basePath;

GLuint fsQuadModelMatrixUniform;
GLuint fsQuadViewMatrixUniform;
GLuint fsQuadProjMatrixUniform;
GLuint fsQuadTextureUniform;

uint16_t windowWidth,windowHeight;

bool updateMode;

int main(int nArgs, char* args[])
{
    basePath=getBasePath();
    //printf("Got Base Path: %s\n",basePath.c_str());
    windowWidth=1280;
    windowHeight=720;

    updateMode = false;
    if (nArgs > 1)
    {
        std::string arg(args[1]);
        if (arg.compare("-u") == 0)
        {
            printf("client in update mode\n");
            updateMode = true;
        }
    }

    if (! glfwInit() )
    {
        printf("ERROR: Could not init GLFW\n");
        exit(0);
    }

    glfwWindowHint(GLFW_SAMPLES, 1);  //antialiasing

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeight, "TextureClient", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);


    //Get the file descriptor that wraps our texture memory from the server
    int textureFileDescriptor;
    int fd;
    struct sockaddr_un addr;
    char messageBuffer[8192];

    bool socketCreated = true;
    bool socketConnected = true;

    //create our IPC socket

    if ((fd = socket(PF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        printf("Socket Create Error!\n");
        socketCreated=false;
    }

    if ( socketCreated)
    {
        memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, SERVER_SOCK_FILE);
        if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
        {
            printf("Socket Connect error!\n");
            socketConnected = false;
        }
    }

    if (socketConnected)
    {
            strcpy (messageBuffer, "Hi2U!");
            if (send(fd, messageBuffer, strlen(messageBuffer)+1, 0) == -1)
            {
                printf("Error sending\n");
            }
            else
            {
                printf ("sent %s\n",messageBuffer);
            }

            textureFileDescriptor = recvFD(fd);
            printf("Got Texture FD %d\n",textureFileDescriptor);
    }

    if (fd >= 0)
    {
        close(fd);
    }



    char* version = (char*)glGetString(GL_VERSION);
    printf("GL Version %s\n",version);

    GLuint importedTexId =0;
    GLuint memoryObject = 0;
    GLint trueVal = GL_TRUE;

    glCreateMemoryObjectsEXT(1,&memoryObject);
    checkGLError("glCreateMemoryObjectsEXT");
    printf("created memory object id: %d\n",memoryObject);
    glMemoryObjectParameterivEXT(memoryObject,GL_DEDICATED_MEMORY_OBJECT_EXT,&trueVal);

    glImportMemoryFdEXT(memoryObject,4096*4096*4,GL_HANDLE_TYPE_OPAQUE_FD_EXT,textureFileDescriptor);
    checkGLError("glImportMemoryFdEXT");

    glGenTextures(1, &importedTexId);
    printf("created imported texture id: %d\n",importedTexId);
    checkGLError("glGenTextures");

    glBindTexture(GL_TEXTURE_2D, importedTexId);
    checkGLError("glBindTexture");
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_TILING_EXT,GL_OPTIMAL_TILING_EXT);
    checkGLError("glTexParameteri");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    checkGLError("glTexParameteri");
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    checkGLError("glTexParameteri");

    glTexStorageMem2DEXT(GL_TEXTURE_2D,1,GL_RGBA8,4096,4096,memoryObject,0);
    checkGLError("glTexStorageMem2DEXT");

    //if this client should write something into the texture, do that.
    if (updateMode)
    {
        int w,h,chans;
        unsigned char* image = stbi_load((basePath + "/mozilla.png").c_str(), &w, &h, &chans,0);
        if(image == nullptr)
        {
            printf("ERROR: Failed to load texture %s/mozilla.png\n",basePath.c_str());
            exit(0);
        }
        printf("Loaded image %d %d %d\n",w,h,chans);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 4096, 4096, GL_RGBA, GL_UNSIGNED_BYTE, image);
        checkGLError("glTexSubImage2D");
    }

    std::vector<char> vertShaderSource;
    std::vector<char> fragShaderSource;

    //create shaders
    if (! loadShader(&vertShaderSource,(basePath + "/fsquad.vert").c_str()))
    {
        printf("ERROR: could not load vert shader! %s/fsquad.vert\n", basePath .c_str());
        exit(0);
    }

    if (! loadShader(&fragShaderSource,(basePath + "/fsquad.frag").c_str()))
    {
        printf("ERROR: could not load frag shader! %s/fsquad.frag\n",basePath.c_str());
        exit(0);
    }

    //create shaders

    GLuint fsVertShaderId = compileShader(GL_VERTEX_SHADER, (GLchar*)vertShaderSource.data());
    GLuint fsFragShaderId = compileShader(GL_FRAGMENT_SHADER, (GLchar*)fragShaderSource.data());

    fsQuadShader = linkShader(fsVertShaderId, fsFragShaderId);

    glDeleteShader(fsFragShaderId);
    glDeleteShader(fsVertShaderId);

    //keep handles to our uniforms for rendering

    fsQuadModelMatrixUniform = glGetUniformLocation(fsQuadShader, "mMatrix");
    fsQuadViewMatrixUniform = glGetUniformLocation(fsQuadShader, "vMatrix");
    fsQuadProjMatrixUniform = glGetUniformLocation(fsQuadShader, "pMatrix");
    fsQuadTextureUniform = glGetUniformLocation(fsQuadShader, "tex");

    //create our full-screen quad geometry

    fsQuad=createQuad();

    //render loop


    glViewport(0,0,windowWidth,windowHeight);

    while(1)
    {
        glfwPollEvents();

        glUseProgram(fsQuadShader);
        glUniformMatrix4fv(fsQuadViewMatrixUniform, 1, false, (GLfloat*)&(glm::mat4(1.0f)[0][0]));
        glUniformMatrix4fv(fsQuadProjMatrixUniform, 1, false, (GLfloat*)&(glm::mat4(1.0f)[0][0]));

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, importedTexId);

        glUniform1i(fsQuadTextureUniform,0);
        glUniformMatrix4fv(fsQuadModelMatrixUniform, 1, false, (GLfloat*)&(fsQuad->modelMatrix[0][0]));

        glBindVertexArray(fsQuad->vaoId);
        glDrawArrays( GL_TRIANGLES, 0, fsQuad->vertices.size() );

        //reset state
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUseProgram(0);

        glFinish();
        glfwSwapBuffers(window);
        //usleep(1000);
    }


    return 0;
}

int recvFD(int socket)
{
    int len;
    int fd;
    char buf[1];
    struct iovec iov;
    struct msghdr msg;
    struct cmsghdr *cmsg;
    char cms[CMSG_SPACE(sizeof(int))];

    iov.iov_base = buf;
    iov.iov_len = sizeof(buf);

    msg.msg_name = 0;
    msg.msg_namelen = 0;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_flags = 0;
    msg.msg_control = (caddr_t) cms;
    msg.msg_controllen = sizeof cms;

    len = recvmsg(socket, &msg, 0);

    if (len < 0) {
        printf("recvmsg failed with error: %s\n", strerror(errno));
        return -1;
    }

    if (len == 0) {
        printf("recvmsg failedwith error: no data\n");
        return -1;
    }

    cmsg = CMSG_FIRSTHDR(&msg);
    memmove(&fd, CMSG_DATA(cmsg), sizeof(int));
    return fd;
}

void checkGLError(char* prefix)
{
    GLenum err = 0 ;
    while( (err = glGetError()) != GL_NO_ERROR )
    {
        printf("GLERROR: %s -  %d\n",prefix,err);
    }
}

GLuint compileShader(GLenum type, const GLchar* source)
{
    GLuint shader = glCreateShader(type);
    GLchar* shaderSource[1]={(GLchar*)source};
    glShaderSource(shader, 1, shaderSource, NULL);
    glCompileShader(shader);
    checkGLError("glCompileShader");

    GLint param;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &param);

    if (!param)
    {
        GLchar errorLog[1024];
        glGetShaderInfoLog(shader, sizeof(errorLog), NULL, errorLog);
        printf("ERROR: compile %s: %s\n", type == GL_FRAGMENT_SHADER ? "frag" : "vert", (char *) errorLog);
        exit(0);
    }

    return shader;
}

GLuint linkShader(GLuint vert, GLuint frag)
{
    GLuint program = glCreateProgram();
    glAttachShader(program, vert);
    glAttachShader(program, frag);
    glLinkProgram(program);
    GLint param;
    glGetProgramiv(program, GL_LINK_STATUS, &param);
    if (! param )
    {
        GLchar errorLog[1024];
        glGetProgramInfoLog(program, sizeof(errorLog), NULL, errorLog);
        printf("ERROR: link: %s\n", (char *)errorLog);
        exit(0);
    }
    return program;
}

bool loadShader(std::vector<char>* buf,const char *file)
{
    FILE *fptr;
    long length;

    fptr = fopen(file, "rb"); /* Open file for reading */
    if (!fptr)
    {
        return false;
    }
    fseek(fptr, 0, SEEK_END);
    length = ftell(fptr);

    buf->resize(length+1); /* Allocate a buffer for the entire length of the file and a null terminator */
    fseek(fptr, 0, SEEK_SET);
    fread((char*)buf->data(), length, 1, fptr);
    fclose(fptr);
    char* terminator;
    terminator=&(buf->at(length));
    *terminator=0;
    return true;
}

Renderable* createQuad()
{
    Renderable* r = new Renderable();

    r->vertices.push_back({glm::vec3(-1.0f,-1.0f,0.0f),glm::vec2(0.0f,1.0f)});  //tl
    r->vertices.push_back({glm::vec3( 1.0f, 1.0f,0.0f),glm::vec2(1.0f,0.0f)});  //br
    r->vertices.push_back({glm::vec3(-1.0f, 1.0f,0.0f),glm::vec2(0.0f,0.0f)});  //bl
    r->vertices.push_back({glm::vec3(-1.0f,-1.0f,0.0f),glm::vec2(0.0f,1.0f)});  //tl
    r->vertices.push_back({glm::vec3( 1.0f,-1.0f,0.0f),glm::vec2(1.0f,1.0f)});  //tr
    r->vertices.push_back({glm::vec3( 1.0f, 1.0f,0.0f),glm::vec2(1.0f,0.0f)});  //br

    glGenBuffers(1, &(r->vboId));
    glBindBuffer(GL_ARRAY_BUFFER, r->vboId);
    glBufferData(GL_ARRAY_BUFFER, r->vertices.size() * sizeof(VertexData) , r->vertices.data(), GL_STATIC_DRAW);

    glGenVertexArrays(1, &(r->vaoId));
    glBindVertexArray(r->vaoId);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GL_FLOAT), 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GL_FLOAT), (GLvoid*)(3*sizeof(GL_FLOAT)));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    //unbind

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    r->modelMatrix=glm::scale(glm::mat4(1.0f),glm::vec3(3.0f,3.0f,1.0f));
    renderables.push_back(r);
    return r;
}

std::string getBasePath()
{
  char pathBuffer[PATH_MAX];
  ssize_t count = readlink( "/proc/self/exe", pathBuffer, PATH_MAX );
  pathBuffer[PATH_MAX-1] = 0; //zero-pad, just in case
  return std::string(dirname(pathBuffer));
}
