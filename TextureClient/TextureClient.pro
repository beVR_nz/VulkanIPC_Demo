TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    gl.c
INCLUDEPATH += /usr/local/include
LIBS += -L/usr/local/lib

LIBS += -lglfw -lGL -ldl

HEADERS += \
    gl3w.h \
    gl.h

DISTFILES += \
    fsquad.vert \
    fsquad.frag

copyfiles.commands += $(COPY_FILE) $$PWD/fsquad.frag $$OUT_PWD;
copyfiles.commands += $(COPY_FILE) $$PWD/fsquad.vert $$OUT_PWD;
copyfiles.commands += $(COPY_FILE) $$PWD/mozilla.png $$OUT_PWD;

first.depends = $(first) copyfiles
export(first.depends)
export(copyfiles.commands)
QMAKE_EXTRA_TARGETS += first copyfiles
